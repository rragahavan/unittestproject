package com.example.loginunittestexample;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Administrator on 03-08-2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class LoginPresenterTest {
    @Mock
    LoginPresenter presenter;
    @Mock
    LoginView view;
    LoginService service;
    @Before
    public void setup()throws Exception
    {
        presenter=new LoginPresenter(view,service);
    }
    @Test
    public void shouldShowErrorNameMessage()throws Exception
    {
        when(view.getUserName()).thenReturn("");
        presenter.onLongclick();
        verify(view).showErrorName(R.string.name_blank);
    }

    @Test
    public void shouldShowErrorPassowrdMessage()throws Exception
    {
        when(view.getUserName()).thenReturn("james");
        when(view.getPassword()).thenReturn("");
        presenter.onLongclick();
        verify(view).showErrorPassword(R.string.password_blank);

    }

}
