package com.example.loginunittestexample;

/**
 * Created by Administrator on 03-08-2016.
 */
public class LoginPresenter {
    private LoginView view;
            LoginService service;
    public LoginPresenter(LoginView view, LoginService service) {
            this.view=view;
            this.service=service;
    }
    public void onLongclick()
    {
        String userName=view.getUserName();
        if(userName.isEmpty())
        {
            view.showErrorName(R.string.name_blank);
        }
        String userPassword=view.getPassword();
        if(userPassword.isEmpty())
        {
            view.showErrorPassword(R.string.password_blank);
        }

    }
}
