package com.example.loginunittestexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements LoginView{
    private Button loginButton;
    private EditText userName,userPassword;
    private LoginPresenter loginPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loginButton=(Button)findViewById(R.id.loginButton);
        userName=(EditText)findViewById(R.id.userNameText);
        userPassword=(EditText)findViewById(R.id.userPassword);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name=userName.getText().toString();
                String password=userPassword.getText().toString();
                boolean login=true;
                if(name.isEmpty())
                {
                    Toast.makeText(LoginActivity.this,R.string.name_blank,Toast.LENGTH_LONG).show();
                    login=false;
                }
                if(password.isEmpty())
                {
                    login=false;
                    Toast.makeText(LoginActivity.this,R.string.password_blank,Toast.LENGTH_LONG).show();
                }
                if(login) {
                    Intent i = new Intent(LoginActivity.this, SecondActivity.class);
                    startActivity(i);
                }
                else
                {
                    Toast.makeText(LoginActivity.this,R.string.login_failure,Toast.LENGTH_LONG).show();
                }
            }

        });
        loginPresenter=new LoginPresenter(this,new LoginService());
    }

    @Override
    public String getUserName() {
        return userName.getText().toString();
    }

    @Override
    public void showErrorName(int resId) {

        userName.setError(getString(R.string.name_blank));
    }

    @Override
    public String getPassword() {
        return userPassword.getText().toString();
    }

    @Override
    public void showErrorPassword(int password_blank) {
        userPassword.setError(getString(R.string.password_blank));
    }
}
