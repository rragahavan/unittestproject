package com.example.loginunittestexample;

/**
 * Created by Administrator on 03-08-2016.
 */
public interface LoginView {
    String getUserName();

    void showErrorName(int resId);

    String getPassword();

    void showErrorPassword(int password_blank);
}
